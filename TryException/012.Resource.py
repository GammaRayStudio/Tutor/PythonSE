from contextlib import contextmanager

filename = input("input filename: ")


class FileReader:
    def __init__(self, filename):
        self.filename = filename

    def __enter__(self):
        self.file = open(self.filename, 'r')
        return self.file

    def __exit__(self, type, msg, traceback):
        self.file.close()
        return False


try:
    with FileReader(filename) as f:
        print("filename : ", filename)
        print("lines : ", len(f.readlines()))
except FileNotFoundError:
    print("Can't find file")

print(" - - - - - - ")


@contextmanager
def file_reader(filename):
    try:
        f = open(filename, 'r')
        yield f  # yield 的物件將作為 as 的值
    finally:
        f.close


with file_reader(filename) as f:
    print("filename : ", filename)
    print("lines : ", len(f.readlines()))
