filename = input("input filename: ")

try:
    print("filename :", filename)
    f = open(filename, 'r')
except FileNotFoundError as err:
    print("Can't find file")
else:
    try:
        line = len(f.readlines())
        print("total lines :", line)
    finally:
        f.close()
        print("finally")

print("\n- - - - - -\n")


def funcA(flag):
    try:
        if flag:
            return 1
    finally:
        print("finally")
    return 0


msg = funcA(True)
print(msg)
