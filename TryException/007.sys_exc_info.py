import sys


def funcA():
    raise Exception("Something Happened.")


try:
    funcA()
except:
    print(sys.exc_info(), "\n")

    exType, exObj, exTrace = sys.exc_info()
    print("Type : ", exType)
    print("Object : ", exObj)

    print("\nTrace : \n")
    while exTrace:
        code = exTrace.tb_frame.f_code

        print("File Name : ", code.co_filename)
        print("Func or File Name : ", code.co_name)

        exTrace = exTrace.tb_next
        print("\n- - - - - - - -\n")
