nums = input("please input number :").split(' ')
try:
    lstNum = [int(num) for num in nums]
except ValueError as err:
    print(err)
else:
    print("average => ", sum(lstNum) / len(lstNum))
