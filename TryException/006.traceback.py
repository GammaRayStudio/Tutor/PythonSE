import traceback
import sys


def funcA():
    text = None
    return text.upper()


def funcB():
    funcA()


def funcC():
    funcB()


try:
    funcC()
except:
    traceback.print_exc()
    traceback.print_exc(file=open('traceback.txt', 'w+'))
    print(sys.exc_info())
