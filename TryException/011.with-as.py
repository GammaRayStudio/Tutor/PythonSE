filename = input("input filename: ")

try:
    with open(filename, 'r') as f:
        print("filename : ", filename)
        print("lines : ", len(f.readlines()))
except FileNotFoundError:
    print("Can't find file")
