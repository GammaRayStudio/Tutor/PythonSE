print(" - - - #1 - - - \n")


def KelvinToFahrenheit(Temperature):
    assert (Temperature >= 0), "Colder than absolute zero!"
    return ((Temperature-273)*1.8)+32


# print(KelvinToFahrenheit(273))
# rint(int(KelvinToFahrenheit(505.78)))
# rint(KelvinToFahrenheit(-5))


print(" - - - #2 - - - \n")

try:
    fh = open("testfile", "w")
    fh.write("This is my test file for exception handling!!")
except IOError:
    print("Error: can\'t find file or read data")
else:
    print("Written content in the file successfully")
    fh.close()

print(" - - - #3 - - - \n")

try:
    fh = open("testfile", "r")
    fh.write("This is my test file for exception handling!!")
except IOError:
    print("Error: can\'t find file or read data")
else:
    print("Written content in the file successfully")


print(" - - - #4 - - - \n")

try:
    fh = open("testfile", "w")
    fh.write("This is my test file for exception handling!!")
finally:
    print("Error: can\'t find file or read data")
    fh.close()

print(" - - - #5 - - - \n")
try:
    fh = open("testfile", "w")
    try:
        fh.write("This is my test file for exception handling!!")
    finally:
        print("Going to close the file")
        fh.close()
except IOError:
    print("Error: can\'t find file or read data")


print(" - - - #6 - - - \n")


def temp_convert(var):
    try:
        return int(var)
    except ValueError as Argument:
        print("The argument does not contain numbers\n", Argument)


# Call above function here.
temp_convert("xyz")

print(" - - - #7 - - - \n")


def functionName(level):
    if level < 1:
        raise Exception(level)
        # The code below to this would not be executed
        # if we raise the exception
    return level


try:
    l = functionName(-10)
    print("level = ", l)
except Exception as e:
    print("error in level argument", e.args[0])


print(" - - - #8 - - - \n")


class Networkerror(RuntimeError):
    def __init__(self, arg):
        self.args = arg


try:
    raise Networkerror("Bad hostname")
except Networkerror as e:
    print(str(e.args))
