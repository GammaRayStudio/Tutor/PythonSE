print("a / b = ?")
try:
    a = int(input("a = "))
    b = int(input("b = "))
    result = "{} / {} = {}".format(a, b, a/b)
    print("\nresult :")
    print(result)
except ZeroDivisionError:
    print("ZeroDivisionError")  # 除零錯誤
except ArithmeticError:
    print("ArithmeticError")  # 運算錯誤
except:
    print("Exception")
