class CalUtil:

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def checkNumA(self, a):
        if a > 0:
            self.a = a
        else:
            raise ValueError("數字 A 必須大於 0")

    def checkNumAandB(self, a, b):
        if a > b:
            self.a = a
            self.b = b
        else:
            raise CalculateException("數字 B 必須小於 A")

    def calNum(self):
        return self.a / self.b


class CalculateException(Exception):
    def __init__(self, message):
        super().__init__(message)


cal = CalUtil(10, 5)
print(cal.calNum())

try:
    cal.checkNumA(-5)
except ValueError as err:
    print(err)

try:
    cal.checkNumAandB(5, 10)
except CalculateException as err:
    print(err)

print("\n- - - raise - - -\n")


try:
    try:
        cal.checkNumA(-10)
    except ValueError as err:
        raise
except Exception as err:
    print("ValueException => ", err)

print("\n- - - raise from - - -\n")

try:
    try:
        cal.checkNumA(-10)
    except ValueError as err:
        raise CalculateException("數字 A 必須大於 0") from err
except Exception as err:
    print("CalculateException => ", err)
