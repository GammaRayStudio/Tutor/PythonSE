try:
    lstNum = input("Please input number:").split(" ")
    average = sum(int(num) for num in lstNum) / len(lstNum)
    print("average => ", average)
except ValueError as err:
    print("\ninput number error")
except (EOFError, KeyboardInterrupt):
    print("\nUser Interrupt")
except:
    print("\nOther Exception")
