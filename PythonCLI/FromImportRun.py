from package.utils.CalUtil import sum
from package.dao import NumberDAO


number01 = NumberDAO.queryById(1)
number02 = NumberDAO.queryById(2)

print("Number01 : ", number01, " , Number02 : ", number02)

result = sum(number01, number02)

print("result = ", result)