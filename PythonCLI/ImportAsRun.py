import package.utils.CalUtil as cal
import package.dao.NumberDAO as dao


number01 = dao.queryById(1)
number02 = dao.queryById(2)

print("Number01 : ", number01, " , Number02 : ", number02)

result = cal.sum(number01, number02)

print("result = ", result)