import package.utils.CalUtil
import package.dao.NumberDAO


number01 = package.dao.NumberDAO.queryById(1)
number02 = package.dao.NumberDAO.queryById(2)

print("Number01 : ", number01, " , Number02 : ", number02)

result = package.utils.CalUtil.sum(number01, number02)

print("result = ", result)
