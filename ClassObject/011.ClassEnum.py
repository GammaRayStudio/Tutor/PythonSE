from enum import IntEnum, Enum, unique


class Action(IntEnum):
    stop = 0
    start = 1
    jump = 2


@unique
class Member(Enum):
    CEO = "Lv01"
    CTO = "Lv02"
    PM = "Lv03"
    SA = "Lv04"
    RD = "Lv05"


print("Action : ")
for act in Action:
    print("    {} -> {}".format(act.name, act.value))

print("Member :")
for mem in Member:
    print("    {} -> {}".format(mem.name, mem.value))

print("---")
print(Action(1))
print(Action['start'])
print(Action.start)
print("---")

act = Action.start

if Action.start:
    print("Action => Start.")
elif Action.stop:
    print("Action => Stop.")
elif Action.jump:
    print("Action => Jump.")
