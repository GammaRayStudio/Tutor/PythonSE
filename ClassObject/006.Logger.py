class Logger:
    __logger = {}  # 保存已建立的 Logger 實例

    def __new__(clz, name):
        if name not in clz.__logger:  # 如果 dict 中不存在，Logger 就建立
            logger = object.__new__(clz)
            clz.__logger[name] = logger
            return logger
        return clz.__loggers[name]  # 存在的話，傳回對應名稱的 Logger 實例

    def __init__(self, name):
        if name not in vars(self):
            self.name = name  # 設定 Logger 的名稱

    def log(self, message):  # 簡單模擬日誌行為
        print("{} : {}".format(self.name, message))

    def __del__(self):  # 自定義清除相關資源
        print("release => {}".format(self.name))


log01 = Logger("CalUtil")
log01.log("Cal Something. ")

log02 = Logger("AppInfoDao")
log02.log("Select By Condition.")

log03 = Logger("AppInfoService")
log03.log("Call Service.")

print("")

log03 = None
