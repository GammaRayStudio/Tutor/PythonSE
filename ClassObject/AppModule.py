class AppInfo:
    def __init__(self, app_id, app_name, app_version, app_author, app_remark):
        self.__app_id = app_id
        self.__app_name = app_name
        self.__app_version = app_version
        self.__app_author = app_author
        self.__app_remark = app_remark

    def __str__(self):
        return "Application Information - {}".format(self.__app_version)

    def update(self, version):
        if self.__check(version):
            print("\nupdate: new version.")
            self.__app_version = version

    def __check(self, version):
        isNewVersion = False
        if self.__app_version != version:
            isNewVersion = True
        return isNewVersion

    @property
    def app_id(self):
        return self.__app_id

    @property
    def app_name(self):
        return self.__app_name

    @property
    def app_version(self):
        return self.__app_version

    @property
    def app_author(self):
        return self.__app_author

    @property
    def app_remark(self):
        return self.__app_remark

    @app_id.setter
    def app_id(self, app_id):
        self.__app_id = app_id

    @app_name.setter
    def app_name(self, app_name):
        self.__app_name = app_name

    @app_version.setter
    def app_version(self, app_version):
        self.__app_version = app_version

    @app_author.setter
    def app_author(self, app_author):
        self.__app_author = app_author

    @app_remark.setter
    def app_remark(self, app_remark):
        self.__app_remark = app_remark

    @app_remark.deleter
    def app_remark(self):
        del self.__app_remark
