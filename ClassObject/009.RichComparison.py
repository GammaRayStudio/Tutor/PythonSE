class AppInfo:
    def __init__(self, name, version=1.01):
        self.name = name
        self.version = version

    def __eq__(self, appInfo):  # ==
        isEq = False
        if self.name == appInfo.name:
            isEq = True
        else:
            isEq = False
        return isEq

    def __gt__(self, appInfo):  # >
        return self.version > appInfo.version

    def __ge__(self, appInfo):  # >=
        return self.version >= appInfo.version


pythonSE = AppInfo("PythonSE")
flaskSE = AppInfo("FlaskSE")
pythonSEv102 = AppInfo("PythonSE", 1.02)

if pythonSE == flaskSE:
    print("#1 : PythonSE == FlaskSE")
if pythonSE != flaskSE:
    print("#1 : PythonSE != FlaskSE")

if pythonSE == pythonSEv102:
    print("#2 : PythonSE == PythonSEv102")
if pythonSE != pythonSEv102:
    print("#2 : PythonSE != PythonSEv102")

if pythonSEv102 > pythonSE:
    print("#3 : PythonSEv102 > PythonSE")

if pythonSE <= pythonSEv102:
    print("#4 : PythonSE <= PythonSEv102")
