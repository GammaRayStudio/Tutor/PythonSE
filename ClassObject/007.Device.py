class Device:
    def __init__(self, name):
        self.name = name
        self.msg = ""

    def printMsg(self):
        print("{} : {}".format(self.name, self.msg))

    def enterMsg(self, msg):
        self.msg = msg

    def parseData(self):
        print("parse device data . . . ")


class XC101(Device):
    def parseData(self):
        print("parse xc102 data . . .")


class XC102(Device):
    def parseData(self):
        super().parseData()
        print("parse xc102 data . . .")


device = Device("None")
xc101 = XC101("XC101")
xc102 = XC102("XC102")

device.enterMsg("Device , not loading.")
xc101.enterMsg("XC - Version 1.0.1")
xc102.enterMsg("XC - Version 1.0.2")

device.printMsg()
xc101.printMsg()
xc102.printMsg()

device.parseData()
xc101.parseData()
xc102.parseData()

print("---")  # 換行

lstDevice = [xc101, xc102]


def printAllDeviceMsg(lstDevice):
    for device in lstDevice:
        device.printMsg()


printAllDeviceMsg(lstDevice)
