from abc import ABCMeta, abstractmethod


class DataCenter(metaclass=ABCMeta):
    def __init__(self, name):
        self.name = name

    @abstractmethod
    def takeChartInfo(self):
        pass


class OrderDataCenter(DataCenter):
    pass
    # def takeChartInfo(self):
    # print("Get top10 order detail . . .")


order = OrderDataCenter("Order")
order.takeChartInfo()
