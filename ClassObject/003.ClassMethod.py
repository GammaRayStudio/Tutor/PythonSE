class DataCenter:
    def __init__(self, message):
        self.__msg = message
        print(self.__msg)

    def printMsg01(self):
        print("DataCenter => Message 01.")

    def printMsg02():
        print("DataCenter => Message 02.")

    @staticmethod
    def printMsg03():
        print("DataCenter => Message 03.")

    @classmethod
    def printMsg(clz, message):
        clz(message)


dataCenter = DataCenter("DataCenter => init")

# Bound Method : 綁定的方法
dataCenter.printMsg01()

# Unbound Mehod : 未綁定的方法
# dataCenter.printMsg02()


DataCenter.printMsg02()

printMsg = DataCenter.printMsg02

printMsg()


# Static Method : 靜態方法，希望某個方法不被拿來作為綁定方法
dataCenter.printMsg03()

dataCenter.printMsg("DataCenter => ClassMethod")
