class CalUtil:
    PI = 3.14159

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def add(self):
        return self.x + self.y


cal = CalUtil(1, 2)
result = cal.add()
print("cal.add() , result => ", result)

result = CalUtil.add(cal)
print("CalUtil.add(cal) , result => ", result)

print("\ncal.__dict__ => ", cal.__dict__)
print("CalUtil.__dict__ => ", CalUtil.__dict__)

print("\nvars(cal) => ", vars(cal))
print("vars(CalUtil) => ", vars(CalUtil))


print("cal.PI => ", cal.PI)
cal.PI = 3.14
print("\ncal.PI = 3.14")
print("cal.PI => ", cal.PI)
print("CalUtil.PI => ", CalUtil.PI)


def sub(self):
    return self.a - self.b


cal.a = 3
cal.b = 2
CalUtil.sub = sub

result = cal.sub()
print("\ncal.sub() , result => ", result)


print("\nCalUtil.__name__ => ", CalUtil.__name__)
