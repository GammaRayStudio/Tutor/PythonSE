class AppInfoService:
    '''提供應用程式資訊，領域層，服務接口 (API)'''

    def printMsg(self, msg):
        '''打印相關資訊'''
        print("print some message.")

    def __str__(self):
        return "Application Information"


service = AppInfoService()


print("service -> ", service)
