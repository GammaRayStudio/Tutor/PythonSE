# from AppModule import *
import AppModule


# 封裝 : 初始化 , 操作流程 , 內部資料
class Info:
    def __init__(self, app_id, app_name, app_version, app_author, app_remark):
        self.__app_id = app_id
        self.__app_name = app_name
        self.__app_version = app_version
        self.__app_author = app_author
        self.__app_remark = app_remark

    def __str__(self):
        return "Application Information - {}".format(self.__app_version)


appInfo = Info(1, "PythonSE", "1.0.1", "Enoxs",
               "Python - Sample Example")

appInfo.app_id = 3
appInfo.app_version = "1.0.2"

print("#1:")
print("app_id => ", appInfo._Info__app_id)
print("app_name => ", appInfo._Info__app_name)
print("app_version => ", appInfo._Info__app_version)
print("app_author => ", appInfo._Info__app_author)
print("app_remark => ", appInfo._Info__app_remark)

print("\nappInfo.toString() => ", appInfo)

appInfo = AppModule.AppInfo(1, "PythonSE", "1.0.1", "Enoxs",
                            "Python - Sample Example")

appInfo.app_id = 3
appInfo.app_version = "1.0.2"

print("\n#2:")
print("app_id => ", appInfo.app_id)
print("app_name => ", appInfo.app_name)
print("app_version => ", appInfo.app_version)
print("app_author => ", appInfo.app_author)
print("app_remark => ", appInfo.app_remark)

appInfo.update("1.0.3")

print("\nappInfo.toString() => ", appInfo)

del appInfo.app_remark
msg = "\n_AppInfo__app_remark => {}".format(
    '_AppInfo__app_remark' in dir(appInfo)
)
print(msg)
