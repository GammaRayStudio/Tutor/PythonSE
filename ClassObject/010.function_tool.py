from functools import total_ordering


@total_ordering
class DataInfo:
    def __init__(self, value):
        self.value = value

    def __eq__(self, info):
        return self.value == info.value

    def __gt__(self, info):
        return self.value > info.value


d1 = DataInfo(10)
d2 = DataInfo(20)

print("d1 >= d2 => {}".format(d1 >= d2))
print("d1 <= d2 => {}".format(d1 <= d2))
