class CalMath:
    def __init__(self, number, total):
        self.number = number
        self.total = total

    def __add__(self, that):  # +
        return CalMath(
            self.number * that.total + that.number * self.number,
            self.total * that.total
        )

    def __sub__(self, that):  # -
        return CalMath(
            self.number * that.total - that.number * self.number,
            self.total * that.total
        )

    def __mul__(self, that):  # *
        return CalMath(
            self.number * that.number,
            self.total * that.total
        )

    def __truediv__(self, that):
        return CalMath(
            self.number * that.total,
            self.total * that.total
        )

    def __str__(self):
        return "{}/{}".format(self.number, self.total)


x1 = CalMath(1, 2)
x2 = CalMath(2, 3)

print("x1 => ", x1)
print("x2 => ", x2)

result = x1 + x2
print("x1 + x2 => ", result)

result = x1 - x2
print("x1 - x2 => ", result)

result = x1 * x2
print("x1 * x2 => ", result)

result = x1 / x2
print("x1 / x2 => ", result)
