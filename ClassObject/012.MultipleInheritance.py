from abc import ABCMeta, abstractmethod


class Info(metaclass=ABCMeta):
    def printMsg(self):
        print("Info => Message.o")

    @abstractmethod
    def parseData(self):
        pass


class App:
    def printMsg(self):
        print("App => Message.")

    def parseData(self):
        print("parse device data . . . ")


class AppInfo(App, Info):
    pass


appInfo = AppInfo()
appInfo.printMsg()


print(AppInfo.__mro__)
print(AppInfo.__bases__)
