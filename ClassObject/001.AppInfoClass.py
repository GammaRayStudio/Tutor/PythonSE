class AppInfo:
    def __init__(self, app_id, app_name, app_version, app_author, app_remark):
        self.app_id = app_id
        self.app_name = app_name
        self.app_version = app_version
        self.app_author = app_author
        self.app_remark = app_remark

    def __str__(self):
        return "Application Information - {}".format(self.app_version)


appInfo = AppInfo(1, "PythonSE", "1.0.1", "Enoxs",
                  "Python - Sample Example")

appInfo.app_id = 3
appInfo.app_version = "1.0.2"

print("app_id => ", appInfo.app_id)
print("app_name => ", appInfo.app_name)
print("app_version => ", appInfo.app_version)
print("app_author => ", appInfo.app_author)
print("app_remark => ", appInfo.app_remark)

print("appInfo.toString() => ", appInfo)
