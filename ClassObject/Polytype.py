from abc import ABCMeta, abstractmethod


class Device(metaclass=ABCMeta):
    def __init__(self, name, version):
        self.name = name
        self.version = version
        self.msg = ""
        self.echoVersion()

    @abstractmethod
    def echoVersion(self):
        pass

    def printMsg(self):
        print("{} : {}".format(self.name, self.msg))

    def enterMsg(self, msg):
        self.msg = msg

    def parseData(self):
        print("parse device data . . . ")


class XC101(Device):
    def parseData(self):
        print("parse xc102 data . . .")

    def echoVersion(self):
        print("Version : XC101.")


class XC102(Device):
    def parseData(self):
        super().parseData()
        print("parse xc102 data . . .")

    def echoVersion(self):
        print("Version : XC102.")


print("init :")
xc101 = XC101("XC101", "XCv101")
xc102 = XC102("XC102", "XCv102")


lstDevice = [xc101, xc102]

print()
print("All Version : ")


def printAllVersion(lstDevice):
    for device in lstDevice:
        device.echoVersion()


printAllVersion(lstDevice)
