f = open("text.txt", "w")
f.write("Text Text Text")
f.close()

f = open("text.txt", "r")
text = f.read()
print(text)
f.close()

with open("text.txt", "r+") as f:
    text = f.read()
    print(text)
    f.write("\nwith open => " + text)
