f = open("text.txt", "r+")
msg = f.read(3)
print("msg => ", msg)

pos = f.tell()
print("pos => ", pos)

pos = f.seek(0)
msg = f.read(20)
print("msg => ", msg)

f.write("Text Text Text ")
f.flush()
f.seek(0)
msg = f.read(20)
print("msg => ", msg)

f.close()
