# pass

print("start => pass")

isFlag = False
if isFlag:
    print("flag = true")
else:
    pass

print("end => pass")

# break

print("\nstart => break")
lstData = ['001', '002', '003', '004', '005']

for text in lstData:
    if text == '003':
        break
    print("text => ", text)
print("end => break")

# continue

print("\nstart => continue")
lstData = ['001', '002', '003', '004', '005']

for text in lstData:
    if text == '003':
        continue
    print("text => ", text)
print("end => continue")
