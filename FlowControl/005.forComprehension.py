# origin

print("origin : list to list")

lstNum = [10, 20, 30]
lstResult = []

for num in lstNum:
    lstResult.append(num ** 2)

print(lstResult)


# [ n for n in lst ]

print("\nfor Comprehension:")
lstNum = [10, 20, 30]
lstResult = [num ** 2 for num in lstNum]
print(lstResult)

# [ n for n in lst if bool]
print("\nfor Comprehension with condition:")
lstNum = [10, 20, 30]
lstResult = [num ** 2 for num in lstNum if num > 15]
print(lstResult)

# [ n for n in lst for m in lst]
print("\nfor Comprehension x 2 :")
lstX = ['A', 'B', 'C']
lstY = ['001', '002', '003']
lstText = [x + y for x in lstX for y in lstY]
print(lstText)

# lazy evaluation
print("\nlazy evaluation :")
num = sum([n for n in range(1, 100)])
print(num)

num = sum(n for n in range(1, 100))
print(num)


# Collection

lstData = [n for n in range(1, 10)]
print(lstData)

setData = {n for n in range(1, 10)}
print(setData)

lstKey = ['001', '002', '003']
lstVal = ['A', 'B', 'C']
mapData = {key: val for key, val in zip(lstKey, lstVal)}
print(mapData)

data = tuple(n for n in range(1, 10))
print(data)
