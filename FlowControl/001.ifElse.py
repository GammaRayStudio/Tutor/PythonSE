num = 15

if num < 5:
    print(" num < 5")
elif 5 < num < 10:
    print(" 5 < num < 10")
else:
    print("num > 10")

# python - if..else 運算式

num01 = 10
num02 = 15
max = num01 if num01 > num02 else num02
print("max => ", max)
