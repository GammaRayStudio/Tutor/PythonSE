count = 0
while count < 10:
    print("count => ", count)
    count += 1
print("loop => end")


# while else
print("\nwheil .. else:")
count = 1
while count < 5:
    print("count => ", count)
    count += 1
else:
    print("else => end")

# while else break
print("\nwheil .. break .. else :")
count = 1
while count < 5:
    print("count => ", count)
    count += 1
    if count == 3:
        break
else:
    print("else => else")
