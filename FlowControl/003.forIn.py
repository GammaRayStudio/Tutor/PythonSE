lstData = ['001', '002', '003', '004', '005']

for text in lstData:
    print(text)

lstDict = {'001': 'A', '002': 'B', '003': 'C'}

for key, val in lstDict.items():
    print('key = ', key, ' , val = ', val)
for key in lstDict.keys():
    print('key = ', key)
for val in lstDict.values():
    print('val = ', val)


# for(i=0;i<n;i++)
print('\nfor(i=0;i<n;i++:')
for i in range(len(lstData)):
    print("{} => {}".format(i, lstData[i]))

# for in .. else
print('\nfor in .. else:')
for text in lstData:
    print(text)
else:
    print("else => end")

# for in .. break .. else
print('\nfor in .. break .. else:')
for text in lstData:
    print(text)
    if text == '003':
        break
else:
    print("else => end")
