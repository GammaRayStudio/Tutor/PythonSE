import os
from flask import Flask, render_template, request , redirect , url_for 
from werkzeug.utils import secure_filename

userInfo = {
    'name' : '開發者' ,
    'account' : 'DevAuth',
    'password' : '00000000' , 
    'photo' : 'logo.png'
}

# 取得伺服器目前路徑
UPLOAD_FOLDER = os.getcwd() + os.path.sep + 'static/img'

# 檔案型態
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)

# 上傳路徑
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# 限制大小 16MB
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

print('file_path : ' , UPLOAD_FOLDER)

@app.route('/')
@app.route('/user')
def user():
    return render_template('004/file_upload/user.html' , userInfo = userInfo)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        account = request.form['account']        
        file = request.files['file']
        root , extension = os.path.splitext(file.filename)
        filename = account + extension

        if file and allowed_file(file.filename):
            file.save(os.path.join(
                app.config['UPLOAD_FOLDER'], secure_filename(filename)))

        global userInfo
        userInfo['photo'] = filename
        return redirect(url_for('user'))

@app.route('/reset' , methods = ['POST'])
def reset():
    if request.method == 'POST':
        global userInfo
        userInfo['photo'] = 'logo.png'
    return redirect(url_for('user'))

if __name__ == '__main__':
    app.run('0.0.0.0' , debug=True)
