from flask import Flask, render_template, request, jsonify, json
app = Flask(__name__)


@app.route('/')
@app.route('/ajax')
def ajax():
    return render_template('003/003-2.ajax.html')


@app.route('/ajax/read', methods=['GET'])
def output_data():
    if request.method == "GET":
        with open('static/data/003/data.json', 'r') as f:
            data = json.load(f)
            print("data : ", data)
        f.close
        return jsonify(data)  # 直接回傳 data 也可以，都是 json 格式


@app.route('/ajax/write', methods=['POST'])
def input_data():
    if request.method == "POST":
        data = {
            'appInfo': {
                'id': request.form['app_id'],
                'name': request.form['app_name'],
                'version': request.form['app_version'],
                'author': request.form['app_author'],
                'remark': request.form['app_remark']
            }
        }
        print(type(data))
        with open('static/data/003/data.json', 'w') as f:
            json.dump(data, f)
        f.close
        return jsonify(result='OK')


@app.route('/ajax/reset', methods=['POST'])
def reset_data():
    if request.method == "POST":
        text = ""
        with open('static/data/003/origin.json', 'r') as f:
            text = f.read()
        with open('static/data/003/data.json', 'w') as f:
            f.write(text)
        return jsonify(result='OK')


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
