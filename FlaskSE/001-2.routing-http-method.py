from flask import Flask, request

app = Flask(__name__)


@app.route('/api', methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
def http_method():
    if request.method == "GET":
        return 'Http Method => GET'
    elif request.method == "POST":
        return 'Http Method => POST'
    elif request.method == "PUT":
        return 'Http Method => PUT'
    elif request.method == "PATCH":
        return 'Http Method => PATCH'
    elif request.method == "DELETE":
        return 'Http Method => DELETE'


@app.route('/api/get')
def http_get():
    if request.method == "GET":
        return 'Http Method => GET'
    else:
        return 'Http Method => Other'


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
