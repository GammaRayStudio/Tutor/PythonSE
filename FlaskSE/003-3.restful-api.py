from flask import Flask, render_template, request, jsonify, json
app = Flask(__name__)


@app.route('/')
@app.route('/system')
def page():
    return render_template('003/003-3.restful.html')


@app.route('/system/app', methods=['GET', 'POST'])
def system_app():
    if request.method == "GET":
        with open('static/data/003/database.json', 'r') as f:
            data = json.load(f)
            print("data : ", data)
        return jsonify(data)  # 直接回傳 data 也可以，都是 json 格式
    if request.method == "POST":
        appInfo = {
            'id': int(request.form['app_id']),
            'name': request.form['app_name'],
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        data = None
        with open('static/data/003/database.json', 'r') as f:
            data = json.load(f)
            data.append(appInfo)
            print("data : ", data)
        with open('static/data/003/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result='OK')


@app.route('/system/app/<int:app_id>', methods=['GET', 'PUT', 'PATCH', 'DELETE'])
def system_app_id(app_id):
    if request.method == "GET":
        data = None
        with open('static/data/003/database.json', 'r') as f:
            data = json.load(f)
        appInfo = None
        for d in data:
            if d["id"] == app_id:
                appInfo = d
        return jsonify(appInfo)
    if request.method == "PUT":
        appInfo = {
            'id': int(request.form['app_id']),
            'name': request.form['app_name'],
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        data = None
        with open('static/data/003/database.json', 'r') as f:
            data = json.load(f)
        for d in data:
            print("d => ", d)
            if d["id"] == app_id:
                d["id"] = appInfo["id"]
                d["name"] = appInfo["name"]
                d["version"] = appInfo["version"]
                d["author"] = appInfo["author"]
                d["remark"] = appInfo["remark"]
        with open('static/data/003/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result="OK")
    if request.method == "PATCH":
        appInfo = {
            'version': request.form['app_version'],
            'author': request.form['app_author'],
            'remark': request.form['app_remark']
        }
        data = None
        with open('static/data/003/database.json', 'r') as f:
            data = json.load(f)
        for d in data:
            print("d => ", d)
            if d["id"] == app_id:
                d["version"] = appInfo["version"]
                d["author"] = appInfo["author"]
                d["remark"] = appInfo["remark"]
        with open('static/data/003/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result="OK")
    if request.method == "DELETE":
        data = None
        with open('static/data/003/database.json', 'r') as f:
            data = json.load(f)
        index = 0
        for d in data:
            print("d => ", d)
            if d["id"] == app_id:
                break
            index += 1
        data.pop(index)
        with open('static/data/003/database.json', 'w') as f:
            json.dump(data, f)
        return jsonify(result="OK")


@app.route('/system/reset', methods=['POST'])
def reset_data():
    if request.method == "POST":
        text = ""
        with open('static/data/003/app_info.json', 'r') as f:
            text = f.read()
        with open('static/data/003/database.json', 'w') as f:
            f.write(text)
        return jsonify(result='OK')


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
