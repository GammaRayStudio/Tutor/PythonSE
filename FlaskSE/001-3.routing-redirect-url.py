from flask import Flask, request, redirect, url_for

app = Flask(__name__)


@app.route('/navgation/<target>', methods=['GET'])
def navgation(target):
    if request.method == "GET":
        if target == "A":
            return redirect(url_for('moduleA'))
        elif target == "B":
            return redirect(url_for('module', module_type='B'))
        else:
            return redirect(url_for('system_message', msg_type='error'))


@app.route('/module/A', methods=['GET'])
def moduleA():
    if request.method == "GET":
        return "Module A"


@app.route('/module/<module_type>', methods=['GET'])
def module(module_type):
    if request.method == "GET":
        return "Module {}".format(module_type)


@app.route('/system/message/<msg_type>', methods=['GET'])
def system_message(msg_type):
    if request.method == "GET":
        return "System {} => {}".format(msg_type, "Could not found this module.")


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
