from flask import Flask, render_template

app = Flask(__name__)


@app.route('/static/image')
def render_image():
    return render_template('002/002-3.static-image.html')


@app.route('/static/script')
def render_script():
    return render_template('002/002-4.static-script.html')


@app.route('/static/jquery')
def render_jquery():
    return render_template('002/002-5.static-jquery.html')


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
