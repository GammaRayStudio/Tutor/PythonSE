from flask import Flask, render_template, request, redirect, url_for, jsonify, json, make_response, session
app = Flask(__name__)


app.secret_key = 'FlaskSE'

userInfo = {
    'name' : '開發者' ,
    'account' : 'DevAuth',
    'password' : '00000000' , 
    'photo' : 'logo.png'
}

lstOrder = [
    ('PythonSE' , 100),
    ('PythonDoc',200),
    ('PythonGUI',349),
    ('PythonFlask',500),
    ('PythonSQL',799),
    ('PythonOCR',1000)
]

@app.route('/')
@app.route('/home/')
def home():
    isLogin , msg = getStatus()
    return render_template('004/session/home.html', login_status = isLogin , login_msg = msg)

def getStatus():
    isLogin = False
    msg = ''
    if 'isLogin' in session:
        isLogin = session['isLogin']
        if not isLogin :
            session.pop('isLogin', None)
            msg = '帳號或密碼錯誤'
    return isLogin , msg

@app.route('/user' , methods=['GET'])
def user():
    isLogin , msg = getStatus()
    if isLogin:
        return render_template('004/session/user.html' , userInfo = userInfo)
    else :
        return redirect(url_for('home'))

@app.route('/order' , methods=['GET'])
def order():
    isLogin , msg = getStatus()
    if isLogin:
        return render_template('004/session/order.html' , orderInfo = lstOrder)
    else :
        return redirect(url_for('home'))

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        account = request.form['account']
        password = request.form['password']

        if userInfo['account'] == account and userInfo['password'] == password :
            session['account'] = account
            session['isLogin'] = True
        else :
            session['isLogin'] = False
        return redirect(url_for('home'))

@app.route('/logout' , methods=['GET'])
def logout():
    session.pop('account', None)
    session.pop('isLogin', None)
    return redirect(url_for('home'))

@app.route('/reset' , methods=['POST'])
def reset():
    session.pop('account', None)
    session.pop('isLogin', None)
    return jsonify(result='OK')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('004/session/error404.html'), 404

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
