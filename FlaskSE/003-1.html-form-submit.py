from flask import Flask, request, render_template, redirect, url_for
app = Flask(__name__)


@app.route('/')
@app.route('/form')
def formPage():
    return render_template('003/003-1.form.html')


@app.route('/submit', methods=['GET', 'POST'])
def submit():
    if request.method == 'GET':
        user = request.args.get('user')
        print("get : user => ", user)
        return redirect(url_for('success', user=user, action="get"))
    elif request.method == 'POST':
        user = request.form['user']
        print("post : user => ", user)
        return redirect(url_for('success', user=user, action="post"))


@app.route('/success/<action>/<user>')
def success(action, user):
    return 'Http Method : {} <br> User : {}'.format(action, user)


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
