import psutil

pl = psutil.pids()

# print(pl)

for pid in pl:
    app_name = psutil.Process(pid).name()
    if 'Google Chrome' == app_name:
        print('pid :', pid, ' , name :', app_name)
