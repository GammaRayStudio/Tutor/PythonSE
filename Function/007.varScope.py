print("變數範圍 :")
text = '全域變數'


def funcGobal():
    print("#1 : ", text)


def funcLocal():
    text = "區域變數"
    print("#2 : ", text)


funcGobal()
funcLocal()
print("#3 : ", text)

# local()
print("\nlocal() 與 global() 函式 : ")
x = 1


def outerFunc():
    y = 2

    def innerFunc():
        z = 3
        print('inner func locals:', locals())
    innerFunc()
    print('outer func locals:', locals())


outerFunc()
print('local():', locals())
print('global():', globals())


# gobal 宣告
print("\nglobal 關鍵字:")

galX = 100
galY = 200


def funcGlobal():
    global galX, galY
    galX = 200
    galY = 300


funcGlobal()
print("x => ", galX)
print("y => ", galY)

# nonlocal 宣告
print("\nnonlocal 關鍵字:")


def funcLocal():
    locX = 100
    locY = 200

    def local():
        nonlocal locX, locY
        locX = 200
        locY = 300

    local()
    print("x => ", locX)
    print("y => ", locY)


funcLocal()
