def calSum(a, b):
    return a + b


result = calSum(3, 2)
print(" 3 + 2 = ", result)

# default : parameter


def printMsg(name, version="1.0.1"):
    print("name = ", name, " , version = ", version)


printMsg("PythonSE")
printMsg("PythonFlask", "1.0.2")

# notice : object parameter


def genListData(element, lstData=[]):
    lstData.insert(0, element)
    return lstData


lstData = genListData(10)
print(lstData)
lstData = genListData(10, [20, 30])
print(lstData)
lstData = genListData(20)
print(lstData)


def genListAdjust(element, lstData=None):
    lstData = lstData if lstData else []
    lstData.insert(0, element)
    return lstData


lstData = genListAdjust(10)
print(lstData)
lstData = genListAdjust(10, [20, 30])
print(lstData)
lstData = genListAdjust(20)
print(lstData)


def queryByParam(index, name, version):
    print(index, ":", "name = ", name, " , version = ", version)


queryByParam(index=1, name="Python", version="1.0.1")
queryByParam(index=2, version="1.0.1", name="Python")
