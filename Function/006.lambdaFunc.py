# filter => lambda
print("filter => lambda")


def filterData(predicate, lstData):
    result = []
    for data in lstData:
        if predicate(data):
            result.append(data)
    return result


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']

print('大於 6：', filterData(lambda elem: len(elem) > 6, lstData))
print('小於 5：', filterData(lambda elem: len(elem) < 5, lstData))
print('有個 i：', filterData(lambda elem: 'i' in elem, lstData))


# lambda => def func()
print("\nlambda => def func():")

max = (lambda n1, n2: n1 if n1 > n2 else n2)
print(max(10, 5))

# switch => dict / lambda

print("\nswitch => dict / lambda")
score = 86

level = score // 10
{
    10: lambda: print('Perfect'),
    9: lambda: print('A'),
    8: lambda: print('B'),
    7: lambda: print('C'),
    6: lambda: print('D')
}.get(level, lambda: print('E'))()
