def printMsg(name, version, author):
    print("name = {} , version = {} , author = {}".format(name, version, author))


print("input : list")

lstData = ["PythonSE", "1.0.1", "Enoxs"]
print("list => ")
printMsg(*lstData)

print("tuple => ")
lstData = ("PythonSE", "1.0.1", "Enoxs")
printMsg(*lstData)

print("\ninput : param x n")


def calTotal(*numbers):
    total = 0
    for num in numbers:
        total += num
    return total


result = calTotal(1, 2, 3, 4, 5)
print("result => ", result)


print("\ninput : dict")


def printDictMsg(name, version, author):
    print("name = {} , version = {} , author = {}".format(name, version, author))


mapData = {'name': 'PythonSE', 'version': '1.0.2', 'author': 'Enoxs'}
printDictMsg(**mapData)

print("\nreceive : dict param")


def queryByParam(**param):
    msg = "name = {} , version = {} , author = {}".format(
        param['name'],
        param['version'],
        param['author']
    )
    print(msg)


queryByParam(name="PythonSE", version="1.0.3", author="Enoxs")

print("\ninput : Any")


def inputAny(*var01, **var02):
    if len(var01) > 0:
        print("list : ", var01)
    if len(var02) > 0:
        print("dict : ", var02)


inputAny(1, 2, 3)
inputAny(a='001', b='002', c='003')
