# filter #1
print("filter #1 :")


def filterData(predicate, lstData):
    result = []
    for data in lstData:
        if predicate(data):
            result.append(data)
    return result


def lenGreaterThan6(lstData):
    return len(lstData) > 6


def lenLessThan5(lstData):
    return len(lstData) < 5


def hasCharI(lstData):
    return 'i' in lstData


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print('大於 6：', filterData(lenGreaterThan6, lstData))
print('小於 5：', filterData(lenLessThan5, lstData))
print('有個 i：', filterData(hasCharI, lstData))

# filter #2
print("\nfilter #2 :")


def filterFunc(predicate, lstData):
    result = []
    for elem in lstData:
        if predicate(elem):
            result.append(elem)
    return result


def lenGreaterThan(num):
    def lenGreaterThanNum(elem):
        return len(elem) > num
    return lenGreaterThanNum


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print('大於 5：', filterFunc(lenGreaterThan(5), lstData))
print('大於 7：', filterFunc(lenGreaterThan(7), lstData))


# map
print("\nmap :")


def mapData(mapper, lstData):
    result = []
    for ele in lstData:
        result.append(mapper(ele))
    return result


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print(mapData(str.upper, lstData))
print(mapData(len, lstData))


# filter map
print("\ndefault : filter()、map()")


def lenGreaterThan(num):
    def lenGreaterThanNum(elem):
        return len(elem) > num
    return lenGreaterThanNum


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print(list(filter(lenGreaterThan(6), lstData)))
print(list(map(len, lstData)))
