def printMsg(name, message):
    def printMsgFormat01():
        print("#01 => ", "name : ", name, ", text : ", message)

    def printMsgFormat02():
        print("#02 => ", "name : ", name, ", text : ", message)

    def printMsgFormat03():
        print("#03 => ", "name : ", name, ", text : ", message)

    if name == "title01":
        printMsgFormat01()
    elif name == "title02":
        printMsgFormat02()
    elif name == "title03":
        printMsgFormat03()


printMsg("title01", "message01")
printMsg("title02", "message02")
printMsg("title03", "message03")
