def calHalfLess50(number):
    result = number / 2
    if result > 50:
        result = calHalfLess50(result)

    return result


num = 120
result = calHalfLess50(num)
print("calHalfLess50 => ", result)


def calGCD(a, b):
    result = 0
    if a % b == 0:
        result = b
    else:
        result = calGCD(b, a % b)
    return result


num01 = 50
num02 = 100
result = calGCD(num01, num02)
print("calGCD => ", result)
