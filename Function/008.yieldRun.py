def xrange(n):
    x = 0
    while x != n:
        yield x
        x += 1


for n in xrange(10):
    print(n)


# generator

print("\ngenerator :")
g = xrange(10)
print("type => ", type(g))
print("next(0) => ", next(g))
print("next(1) => ", next(g))


# producer <-> consumer

print("\nproducer <-> consumer :")
count = 0


def producer():
    while True:
        global count
        count += 1
        print('producer ：', count)
        yield count


def consumer():
    while True:
        count = yield
        print('consumer ：', count)


def clerk(jobs, producer, consumer):
    print('run {} time'.format(jobs))
    p = producer()
    c = consumer()
    next(c)
    for i in range(jobs):
        count = next(p)
        c.send(count)


clerk(3, producer, consumer)
