def supportImport():
    import support
    print("x => ", support.x)
    print("y => ", support.y)
    print("local() => ", locals())


def isSupportExist():
    import sys
    return 'support' in sys.modules


supportImport()
print("global() => ", globals())
