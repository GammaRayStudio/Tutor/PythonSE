import sys
import support
import module

isSupportExist = 'support' in sys.modules
isModuleExist = 'module' in sys.modules

print("isSupportExist => ", isSupportExist)
print("isModuleExist => ", isModuleExist)
