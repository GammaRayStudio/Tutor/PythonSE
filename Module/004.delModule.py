import support
from support import x


isExist = 'support' in dir()
print("Support => ", isExist)

del support

isExist = 'support' in dir()
print("Support => ", isExist)


isExist = 'x' in dir()
print("x => ", isExist)

del x

isExist = 'x' in dir()
print("x => ", isExist)

# import support as sup
if 'support' not in dir():
    import support
    sup = support
    print('sup.x => ', sup.x)
