from support import x
from support import y
from support import text
import support as sup
import support

x = 500
y = 500

# create new variable
print("support.x => ", support.x)
print("support.y => ", support.y)
print("sup.x => ", sup.x)
print("sup.y => ", sup.y)

# modify object
print("text => ", text)

text[0] = "AAA"
print("support.text => ", support.text)
print("sup.text => ", sup.text)
