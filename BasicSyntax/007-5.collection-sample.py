# id , name , version , author , date , remark
lstData = [
    (1, 'PythonSE', '1.0.1', 'DevAuth', '2021-01-01', 'Python - Simple Example'),
    (2, 'PythonDoc', '1.0.2', 'DevAuth', '2021-01-05', 'Python - Document'),
    (3, 'FlaskSE', '1.0.1', 'Develop', '2021-01-10', 'Flask - Simple Example'),
    (4, 'PythonSQL', '1.0.2', 'Develop', '2021-01-15', 'Python - SQL Database'),
    (5, 'PythonOCR', '1.0.3', 'Enoxs', '2021-01-20', 'Python - OCR Text')
]

# 計數器
print("計數器")

countMap = {}

for data in lstData:
    row_id, name, version, author, date, remark = data
    if countMap.get(version) == None:
        countMap[version] = 1
    else:
        count = countMap.get(version)
        count += 1
        countMap[version] = count

print(countMap)

# 排序
print("自訂排序")
countMap = {}
for data in lstData:
    row_id, name, version, author, date, remark = data
    countMap[row_id] = data

lstSort = [3, 2, 1, 5, 4]
lstInfo = []
for row_id in lstSort:
    row_id, name, version, author, date, remark = countMap[row_id]
    msg = "row_id : {} -> {}".format(row_id, name)
    # lstInfo.append(msg)
    print(msg)

# print(lstInfo)

lstId = []

# 過濾器
for data in lstData:
    row_id, name, version, author, date, remark = data
    lstId.append(version)

lstFilter = set(lstId)
lstSort = list(lstFilter)
lstSort.sort()

print(lstId)
print(lstFilter)
print(lstSort)
