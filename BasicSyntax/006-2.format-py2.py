str = "Hello %s " % "World"
print(str)

str = "Price : %d " % 1000
print(str)

str = "Price : %f" % 1000
print(str)

str = "%3d / %3d = %.2f" % (10 , 3 , 10/3)
print(str)

str = "%-3d / %-3d = %.2f" % (10 , 3 , 10/3)
print(str)

str = "%-3d / %-3d = %10.2f" % (10 , 3 , 10/3)
print(str)
