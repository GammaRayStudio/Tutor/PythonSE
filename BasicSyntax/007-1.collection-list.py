lstData = [1 , 2 , 3]
print(lstData)

lstData.append(4)
print(lstData)

print("index : ")

for i in range(0,len(lstData)):
    print(" {} => {}".format(i,lstData[i]))


lstData[0] = 0
print(lstData)

lstData.remove(0)
print(lstData)

del lstData[0]
print(lstData)

lstData.extend([5,6,7])
print(lstData)

length = len(lstData)
print(length)

hadFive = 5 in lstData 
print("5 ? => " , hadFive)


# list()
lstData = list("ABCDEF")
print(lstData)

lstData = list((1,2,3,4,5))
print(lstData)

lstData = list({'A' , 'B' , 'C' , 'A' , 'B'})
print(lstData)


# empty
lstData = []
print(lstData)


# diff data
lstData = [1 , 'text' , True]
print(lstData)