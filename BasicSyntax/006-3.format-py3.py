import sys
str = '{} / {} = {}'.format(10,3,10/3)
print(str)
str = '{2} / {1} = {0}'.format(10/3,3,10)
print(str)
str = '{n1} / {n2} = {result}'.format(result = 10/3 , n1 = 10 , n2 = 3)
print(str)


str = '{0:d} / {1:d} = {2:f}'.format(10,3,10/3)
print(str)
str = '{0:5d} / {1:5d} = {2:10.2f}'.format(10,3,10/3)
print(str)
str = '{n1:5d} / {n2:5d} = {result:.2f}'.format(result = 10/3 , n1 = 10 , n2 = 3)
print(str)
str = '{n1:<5d} / {n2:<5d} = {result:.2f}'.format(result = 10/3 , n1 = 10 , n2 = 3)
print(str)
str = '{n1:>5d} / {n2:>5d} = {result:.2f}'.format(result = 10/3 , n1 = 10 , n2 = 3)
print(str)
str = '{n1:x^5d} / {n2:x^5d} = {result:.2f}'.format(result = 10/3 , n1 = 10 , n2 = 3)
print(str)

lstData = ['A' , 'B' , 'C']
str = "Code : {lst[0]} , {lst[1]} , {lst[2]}".format(lst = lstData)
print(str)

mapData = {'A' : '001' , 'B' : '002' , 'C' : '003'}
str = "Type A : {map[A]}".format(map = mapData)
print(str)

str = "platform : {pc.platform}".format(pc = sys)
print(str)