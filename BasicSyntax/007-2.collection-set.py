lstData = {"A" , "B" , "C" , "A" , "B"}
print(lstData)


# 空集合
lstData = set()
lstData.add("001")
lstData.add("002")
lstData.add("003")
print(lstData)

lstData.remove("001")
print(lstData)

isCode001 = "001" in lstData
print(isCode001)

# 內容元素
lstData = {1 , 'text' , True}
print(lstData)

# set() 函式
lstData = set('ABCAB')
print(lstData)

lstData = set([1,2,3,2,1])
print(lstData)

lstData = set((1,2,3,2,1))
print(lstData)

