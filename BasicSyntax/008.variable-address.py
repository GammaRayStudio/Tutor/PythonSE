# id()
x = 1.0 
y = x
print(id(x) , id(y))

y = 2.0
print(id(x) , id(y))

# update address
x = [1,2,3]
y = x
x[0] = 10
print(y)

# check
list01 = [1,2,3]
list02 = [1,2,3]

isEqualsValue = list01 == list02
print("isEquals = " , isEqualsValue)

isSameAddress = list01 is list02
print("isSameAddress = " , isSameAddress)