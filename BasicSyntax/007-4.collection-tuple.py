lstData = 1 , 'text' , False
print(lstData)

lstData = (2 , 'text' , True)
print(lstData)

# Unpack
id , content , isUsed = lstData
print("id = " , id)
print("content = " , content)
print("isUsed = " , isUsed)

# Swip , Python is Cool !
x = 10
y = 20 

(x,y) = (y,x)
print("x = " , x)
print("y = " , y)

# Extended Iterable Unpacking *
(a , *b) = (1,2,3,4,5)
print("a = " , a)
print("b = " , b)

(a , *b , c) = (1,2,3,4,5)
print("a = " , a)
print("b = " , b)
print("c = " , c)
