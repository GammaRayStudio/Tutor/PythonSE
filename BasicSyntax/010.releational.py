a = 10
b = 20

print("a == b : " , (a == b))
print("a != b : " , (a != b))
print("a > b : " , (a > b))
print("a < b : " , (a < b))
print("b >= a : " , (b >= a))
print("b <= a : " , (b <= a))

# Pyhton : Feature
c = 30
print("a < b < c : " , (a < b < c) )
print("a < b and b < c : " , (a < b and b < c) )


# str
text01 = "ABC"
text02 = "AAC"
print("text01 > text02 : " , text01 > text02)

# list
lstNum01 = [1,2,3]
lstNum02 = [1,1,3]

print("lstNum01 > lstNum02 : " , lstNum01 > lstNum02)

lstText01 = ['A' , 'B' , 'C']
lstText02 = ['A' , 'C' , 'C']
print("lstText01 < lstText02 : " , lstText01 < lstText02)

