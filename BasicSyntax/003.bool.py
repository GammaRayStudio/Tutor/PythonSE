isTrue = True
isFalse = False


if isTrue :
    print("Status01 => True")
else : 
    print("Status01 => False")

if isFalse : 
    print("Status02 => True")
else : 
    print("Status02 => False")

isFlag = bool(0)
print(isFlag)

isFlag = bool(1)
print(isFlag)

isFlag = bool(None) and bool(False) and bool('')
print(isFlag)

isFlag = bool(0) and bool(0.0) and bool(0j) 
print(isFlag)

isFlag = bool(()) and bool([]) and bool({})
print(isFlag)

