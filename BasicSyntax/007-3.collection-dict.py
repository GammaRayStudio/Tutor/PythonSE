map = {"A": "001", "B": "002", "C": "003"}
print(map)
print("key : A => ", map["A"])

map["A"] = "ABC"
print(map)

str = map.get("A")
print(str)

del map["A"]
print(map)

# check
isKey = "B" in map
print(isKey)

if map.get("A") == None:
    print("Key A is not exist.")

msg = map.get("D", "Default Data")
print(msg)

# iter
map = {"A": "001", "B": "002", "C": "003"}

lstData = list(map.items())
print(lstData)

lstData = list(map.keys())
print(lstData)

lstData = list(map.values())
print(lstData)

# dict() 
map = dict(a1=101, a2=102, a3=103)
print(map)

map = dict([('b1', 101), ('b2', 102), ('b3', 103)])
print(map)

map = dict.fromkeys(['c1', 'c2'], 100)
print(map)

