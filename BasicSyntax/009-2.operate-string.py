name01 = "Gamma "
name02 = "Ray "
name03 = "Studio"

name = name01 + name02 + name03
print(name)

name = name01 * 3
print(name)

# string + integer
version = 2.0
name = "App " + str(version)
print(name)
