text = "Gamma Ray Studio"
# [start:end]
print("[start:end]")
print(text[0:5])
print(text[6:])
print(text[:10])
print(text[:-7])
print(text[-10:-7])
print("\n")

# shallow copy
print("shallow copy")
lstData = [1,2,3,4,5]
print(lstData[0:3])

lstData[0] = 0
print(lstData[0:3])

print("\n")

# set value
print("set value")
print(lstData[3:5])
lstData[3:5] = [10,10]
print(lstData[3:5])

lstData[3:5] = [100]
print(lstData)

print("\n")

# empty
print("empty")
lstData[:] = []
print(lstData)

print("\n")

# del
print("del")
lstData = [1,2,3,4,5]
del lstData[1:4]
print(lstData)

