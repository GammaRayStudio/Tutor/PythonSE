str01 = "Gamma Ray Studio 's App "
str02 = '"Gamma Ray Studio"'

print(str01)
print(str02)

str03 = 'Gamma Ray Studio \'s App '
str04 = "\"Gamma Ray Studio\""
str05 = "Gamma \\ Ray"

print(str03)
print(str04)
print(str05)

strOct = "\101"
strHex = "\x41"
strChar01 = "\u54C8\u56C9"
strChar02 = "\U000054C8\U000056C9"

print(strOct)
print(strHex)
print(strChar01)
print(strChar02)

strPi = str(3.14)
strCode = ord('A')
strChar = chr(65)

print(strPi)
print(strCode)
print(strChar)


text = """
Gamma Ray Ray :
Text Text Text
Text Text Text
Text Text Text
"""

print(text)



msg = "Gamma Ray Studio"
print(msg[0] , msg[6] , msg[10])

isGinMsg = 'G' in msg
print(isGinMsg)
