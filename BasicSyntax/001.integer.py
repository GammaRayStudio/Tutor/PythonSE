numDec = 10
numBin = 0b1010
numOct = 0o12
numHex = 0x0A

print(type(numDec))
print(type(numBin))
print(type(numOct))
print(type(numHex))

strDec = int("10")
numPi = int(3.14)
boolTrue = int(True)
boolFalse = int(False)
strOct = int('12',8)
strHex = int('a',16)

print(strDec)
print(numPi)
print(boolTrue)
print(boolFalse)
print(strOct)
print(strHex)

strBin = bin(10)
strOct = oct(10)
strHex = hex(10)

print(strBin)
print(strOct)
print(strHex)
