a = 10
b = 5
c = 0

c = a + b
print ("{} + {} = {} ".format(a , b , c))

c = a - b
print ("{} - {} = {} ".format(a , b , c))

c = a * b
print ("{} * {} = {} ".format(a , b , c))

c = a / b
print ("{} / {} = {} ".format(a , b , c))

c = a % b
print ("{} % {} = {} ".format(a , b , c))

a = 2
b = 3
c = a**b 
print("指數運算 : {} ** {} = {}".format(a,b,c))

a = 10
b = 3
c = a//b 
print ("小數點捨去 : {} // {} = {}".format(a,b,c))